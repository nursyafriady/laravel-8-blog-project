<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    HomeController, 
    AdminController, 
    CategoryController, 
    PostController,
    SettingController
};


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/detail/{slug}/{id}', [HomeController::class, 'detail'])->name('detail');
Route::get('/all-categories', [HomeController::class, 'all_category'])->name('all-category');
Route::get('/category/{slug}/{id}', [HomeController::class, 'category'])->name('category');
Route::post('/save-comment/{slug}/{id}', [HomeController::class, 'save_comment'])->name('save-comment');
Route::get('/save-post-form', [HomeController::class, 'save_post_form'])->name('save-post');
Route::post('/save-post-form', [HomeController::class, 'save_post_data'])->name('save-post-data');
Route::get('/manage-posts', [HomeController::class, 'manage_posts'])->name('manage-posts');

// Admin
Route::get('/admin/login', [AdminController::class, 'login'])->name('login');
Route::post('/admin/login', [AdminController::class, 'submit_login']);
Route::get('/admin/logout', [AdminController::class, 'logout'])->name('logout');
Route::get('/admin/dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');

// Comments
Route::get('/admin/comment', [AdminController::class, 'comments'])->name('comments');
Route::get('/admin/comment/delete/{id}', [AdminController::class, 'delete_comment'])->name('delete-comment');

// Users
Route::get('/admin/user', [AdminController::class, 'users'])->name('users');
Route::get('/admin/user/delete/{id}', [AdminController::class, 'delete_user'])->name('delete-user');

// Categories
Route::get('admin/category/{id}/delete',[CategoryController::class,'destroy']);
Route::resource('/admin/category', CategoryController::class);

// Posts
Route::get('admin/post/{id}/delete',[PostController::class,'destroy']);
Route::resource('/admin/post', PostController::class);

// Settings
Route::get('/admin/setting', [SettingController::class, 'index'])->name('setting');
Route::post('/admin/setting', [SettingController::class, 'save_settings'])->name('save_setting');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/', function () {
//     return view('welcome');
// });
