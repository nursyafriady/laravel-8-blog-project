@extends('layouts.default')

@section('title', 'Save Post')
@section('content')
    <main class="container mt-5">
        <div class="row">
            <div class="col-md-8">
                <h3>Add Post</h3>
                <div class="table-responsive">

                @if($errors)
                  @foreach($errors->all() as $error)
                    <p class="text-danger">{{$error}}</p>
                  @endforeach
                @endif

                @if(Session::has('success'))
                  <p class="text-success">{{session('success')}}</p>
                @endif

                <form method="post" action="{{ url('save-post-form') }}" enctype="multipart/form-data">
                  @csrf
                  <table class="table table-bordered">
                      <tr>
                          <th>Pilih Category <span class="text-danger">*</span> </th>
                          <td>
                            <select class="form-control" name="category">
                              @foreach($categories as $cat)
                                  <option value="{{ $cat->id }}">{{ $cat->title }}</option>
                              @endforeach
                            </select>
                          </td>
                      </tr>
                      <tr>
                          <th>Title <span class="text-danger">*</span> </th>
                          <td><input type="text" name="title" class="form-control" value="{{ old('title') }}"/></td>
                      </tr>
                      <tr>
                          <th>Tags</th>
                          <td><input type="text" name="tags" class="form-control" value="{{ old('tags') }}"/></td>
                      </tr>
                      <tr>
                          <th>Detail <span class="text-danger">*</span></th>
                          <td>
                            <textarea name="detail" class="form-control" type="text" rows="10" cols="100" style="resize:none">
                              {{ old('detail') }}
                            </textarea>
                          </td>
                      </tr>
                      <tr>
                          <th>Thumbnail</th>
                          <td><input type="file" name="thumbnail" /></td>
                      </tr>
                      <tr>
                          <th>Full Image</th>
                          <td><input type="file" name="full_image" /></td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <input type="submit" class="btn btn-primary" />
                          </td>
                      </tr>
                  </table>
                </form>
              </div>
            </div>

            <!-- Right Sidebar -->
            <div class="col-md-4">
                <!-- Search -->
                <div class="card mb-3">
                    <h5 class="card-header">Search</h5>
                    <div class="card-body">
                        <form action="{{ route('home') }}">
                            <div class="input-group">
                                <input type="text" class="form-control" name="q">
                                <div class="input-group-append">
                                    <button class="btn btn-dark" type="button" id="button-addon2">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Recent Post -->
                <div class="card mb-3">
                    <h5 class="card-header">Recent Post</h5>
                    <div class="list-group list-group-flush">
                        @if($recent_posts)
                            @foreach($recent_posts as $post)
                                <a href="#" class="list-group-item">{{ $post->title }}</a>
                            @endforeach
                        @endif
                        <!-- <a href="#" class="list-group-item">Post 2</a>
                        <a href="#" class="list-group-item">Post 3</a> -->
                    </div>
                </div>
                <!-- Popular Post -->
                <div class="card mb-3">
                    <h5 class="card-header">Popular Post</h5>
                    <div class="list-group list-group-flush">
                        <a href="#" class="list-group-item">Post 1</a>
                        <a href="#" class="list-group-item">Post 2</a>
                        <a href="#" class="list-group-item">Post 3</a>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection