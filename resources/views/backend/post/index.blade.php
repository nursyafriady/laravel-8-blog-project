 @extends('layouts.admin')
 @section('title', 'All Post')
 
 @section('content')
      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.html">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol> -->

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i> Posts
              <a href="{{ url('admin/post/create') }}" class="float-right btn btn-sm btn-dark">Add Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Category</th>
                      <th>Thumbnail</th>
                      <th>Full Image</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($data as $key=>$post)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->category->title ? $post->category->title : ''}}</td>
                        <td>
                          @if($post->thumbnail)
                            <img src="{{ asset('imgs/thumbnail').'/'.$post->thumbnail }}" width="100" />
                          @else
                            <p>N / A</p>
                          @endif 
                        </td>
                        <td>
                          @if($post->full_image)
                            <img src="{{ asset('imgs/fullimage').'/'.$post->full_image }}" width="100" />
                          @else
                            <p>N / A</p>
                          @endif 
                        </td>
                        <td>
                          <a class="btn btn-info btn-sm" href="{{url('admin/post/'.$post->id.'/edit')}}">Update</a>
                          <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-danger btn-sm" href="{{url('admin/post/'.$post->id.'/delete')}}">Delete</a>
                        </td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- /.content-wrapper -->
@endsection