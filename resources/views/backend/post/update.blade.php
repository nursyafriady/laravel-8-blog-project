@extends('layouts.admin')

@section('title', 'Update Post')
@section('content')
    <div id="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i> Update Post
            <a href="{{url('admin/post')}}" class="float-right btn btn-sm btn-dark">All Data</a>
          </div>
          <div class="card-body">
            <div class="table-responsive">

              @if($errors)
                @foreach($errors->all() as $error)
                  <p class="text-danger">{{$error}}</p>
                @endforeach
              @endif

              @if(Session::has('success'))
              <p class="text-success">{{session('success')}}</p>
              @endif

              <form method="post" action="{{url('admin/post/'.$data->id)}}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <table class="table table-bordered">
                    <tr>
                        <th>Pilih Category <span class="text-danger">*</span> </th>
                        <td>
                          <select class="form-control" name="category">
                            @foreach($category as $cat)
                                @if($cat->id == $data->category_id)
                                    <option selected value="{{ $cat->id }}">{{ $cat->title }}</option>
                                @else
                                    <option value="{{ $cat->id }}">{{ $cat->title }}</option>
                                @endif
                            @endforeach
                          </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <td><input type="text" value="{{$data->title}}" name="title" class="form-control" /></td>
                    </tr>
                    <tr>
                        <th>Tags</th>
                        <td><input type="text" name="tags" class="form-control" value="{{$data->tags}}"/></td>
                    </tr>
                    <tr>
                        <th>Detail <span class="text-danger">*</span></th>
                        <td>
                          <textarea name="detail" class="form-control" type="text" rows="10" cols="100" style="resize:none">
                            {{$data->detail}}
                          </textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>Thumbnail</th>
                        <td>
                          <p class="my-2"><img width="80" src="{{asset('imgs/thumbnail')}}/{{$data->thumbnail}}" /></p>
                          <input type="hidden" value="{{$data->thumbnail}}" name="thumbnail" />
                          <input type="file" name="thumbnail" />
                        </td>
                    </tr>
                    <tr>
                        <th>Full Image</th>
                        <td>
                          <p class="my-2"><img width="80" src="{{asset('imgs/fullimage')}}/{{$data->full_image}}" /></p>
                          <input type="hidden" value="{{$data->full_image}}" name="full_image" />
                          <input type="file" name="full_image" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" class="btn btn-primary" />
                        </td>
                    </tr>
                </table>
              </form>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content-wrapper -->
@endsection