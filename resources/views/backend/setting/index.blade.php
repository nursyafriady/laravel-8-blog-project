@extends('layouts.admin')
@section('title', 'Setting')

@section('content')
<div id="content-wrapper">
        <div class="container-fluid">
          <!-- DataTables -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i> Manage Setting
            </div>
            <div class="card-body">
              <div class="table-responsive">

                @if($errors)
                  @foreach($errors->all() as $error)
                    <p class="text-danger">{{$error}}</p>
                  @endforeach
                @endif

                @if(Session::has('success'))
                  <p class="text-success">{{session('success')}}</p>
                @endif

                <form method="post" action="{{ url('admin/setting') }}" enctype="multipart/form-data">
                  @csrf
                  <table class="table table-bordered">
                      <tr>
                          <th>Comment Auto Approve</th>
                          <td><input @if($setting) value="{{ $setting->comment_auto_approve }}" @endif type="text" name="comment_auto_approve" class="form-control"/></td>
                      </tr>
                      <tr>
                          <th>User Auto Approve</th>
                          <td><input @if($setting) value="{{ $setting->user_auto_approve }}" @endif type="text" name="user_auto_approve" class="form-control"/></td>
                          
                      </tr>
                      <tr>
                          <th>Recent Post Limit</th>
                          <td><input @if($setting) value="{{ $setting->recent_post_limit }}" @endif type="text" name="recent_post_limit" class="form-control"/></td>
                      </tr>
                      <tr>
                          <th>Popular Post Limit</th>
                          <td><input @if($setting) value="{{ $setting->popular_post_limit }}" @endif type="text" name="popular_post_limit" class="form-control"/></td>
                      </tr>
                      <tr>
                          <th>Recent Comments Limit</th>
                          <td><input @if($setting) value="{{ $setting->recent_comments_limit }}" @endif type="text" name="recent_comments_limit" class="form-control"/></td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <input type="submit" class="btn btn-primary" />
                          </td>
                      </tr>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection