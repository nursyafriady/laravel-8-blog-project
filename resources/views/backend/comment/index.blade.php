@extends('layouts.admin')
 @section('title', 'All Comment')
 
 @section('content')
      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i> Comments
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>User</th>
                      <th>Comment</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($comments as $key=>$comment)
                      <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $comment->user->email ?? 'No Email'  }}</td>
                        <td>{{ $comment->comment }}</td>
                        <td>
                          <a onclick="return confirm('Are you sure you want to delete?')" 
                            class="btn btn-danger btn-sm" href="{{url('admin/comment/delete/'.$comment->id)}}">Delete</a>
                        </td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- /.content-wrapper -->
@endsection