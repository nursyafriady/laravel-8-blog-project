@extends('layouts.default')

@section('title', 'Home Page')
@section('content')
    <main class="container mt-5">
        <div class="row">
            <div class="col-md-8">
                <div class="row mb-5">
                    @if(count($posts) > 0)
                        @foreach($posts as $post)
                        <div class="col-md-4">
                            <div class="card mb-4">
                                <a href="{{ url('detail/'.Str::slug($post->title).'/'.$post->id) }}">
                                    <img 
                                        src="{{ asset('imgs/thumbnail/'.$post->thumbnail) }}" 
                                        class="card-img-top" 
                                        alt="{{ $post->title }}"
                                        style="height: 12rem;"
                                    >
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">
                                        <a href="{{ url('detail/'.Str::slug($post->title).'/'.$post->id) }}">
                                            {{ $post->title }}
                                        </a>
                                    </h5>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <p class="alert alert-danger">Nothing Post Found</p>
                    @endif
                </div>

                <!-- Pagination -->
                {{ $posts->links() }}
            </div>

            <!-- Right Sidebar -->
            <div class="col-md-4">
                <!-- Search -->
                <div class="card mb-3">
                    <h5 class="card-header">Search</h5>
                    <div class="card-body">
                        <form action="{{ route('home') }}">
                            <div class="input-group">
                                <input type="text" class="form-control" name="q">
                                <div class="input-group-append">
                                    <button class="btn btn-dark" type="button" id="button-addon2">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Recent Post -->
                <div class="card mb-3">
                    <h5 class="card-header">Recent Post</h5>
                    <div class="list-group list-group-flush">
                        @if($recent_posts)
                            @foreach($recent_posts as $post)
                                <a href="#" class="list-group-item">{{ $post->title }}</a>
                            @endforeach
                        @endif
                        <!-- <a href="#" class="list-group-item">Post 2</a>
                        <a href="#" class="list-group-item">Post 3</a> -->
                    </div>
                </div>
                <!-- Popular Post -->
                <div class="card mb-3">
                    <h5 class="card-header">Popular Post</h5>
                    <div class="list-group list-group-flush">
                        @if($popular_posts)
                            @foreach($popular_posts as $post)
                                <a href="#" class="list-group-item">
                                    {{ $post->title }}
                                    <span class="badge badge-info float-right">{{ $post->views }}</span>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection