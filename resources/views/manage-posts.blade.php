@extends('layouts.default')

@section('title', 'Manage Post')
@section('content')
    <main class="container mt-5">
        <div class="row">
            <div class="col-md-8">
                <h3>Manage Post</h3>
                <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Category</th>
                      <th>Thumbnail</th>
                      <th>Full Image</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($data as $key=>$post)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->category->title ? $post->category->title : ''}}</td>
                        <td>
                          @if($post->thumbnail)
                            <img src="{{ asset('imgs/thumbnail').'/'.$post->thumbnail }}" width="100" />
                          @else
                            <p>N / A</p>
                          @endif 
                        </td>
                        <td>
                          @if($post->full_image)
                            <img src="{{ asset('imgs/fullimage').'/'.$post->full_image }}" width="100" />
                          @else
                            <p>N / A</p>
                          @endif 
                        </td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>

            <!-- Right Sidebar -->
            <div class="col-md-4">
                <!-- Search -->
                <div class="card mb-3">
                    <h5 class="card-header">Search</h5>
                    <div class="card-body">
                        <form action="{{ route('home') }}">
                            <div class="input-group">
                                <input type="text" class="form-control" name="q">
                                <div class="input-group-append">
                                    <button class="btn btn-dark" type="button" id="button-addon2">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Recent Post -->
                <div class="card mb-3">
                    <h5 class="card-header">Recent Post</h5>
                    <div class="list-group list-group-flush">
                        @if($recent_posts)
                            @foreach($recent_posts as $post)
                                <a href="#" class="list-group-item">{{ $post->title }}</a>
                            @endforeach
                        @endif
                        <!-- <a href="#" class="list-group-item">Post 2</a>
                        <a href="#" class="list-group-item">Post 3</a> -->
                    </div>
                </div>
                <!-- Popular Post -->
                <div class="card mb-3">
                    <h5 class="card-header">Popular Post</h5>
                    <div class="list-group list-group-flush">
                    @if($popular_posts)
                            @foreach($popular_posts as $post)
                                <a href="#" class="list-group-item">{{ $post->title }}</a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection