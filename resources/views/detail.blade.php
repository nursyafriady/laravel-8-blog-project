@extends('layouts.default')

@section('title', $detail->title)
@section('content')
<main class="container mt-5">
    <div class="row">
        <div class="col-md-8">
            @if(Session::has('success'))
                <p class="text-success">{{ session('success') }}</p>
            @endif
            <div class="card">
                <h5 class="card-header">
                    {{ $detail->title }}
                    <span class="float-right">Dilihat : {{ $detail->views }}</span>
                </h5>
                <img 
                    src="{{ asset('imgs/fullimage/'.$detail->full_image) }}" 
                    class="card-img-top" 
                    alt="{{ $detail->title }}"
                    style="object-fit:cover; background-size: cover; height: 250px; "
                >
                <div class="card-body">
                    {{ $detail->detail }}
                </div>
                <div class="card-footer">
                   In <a href="{{ url('category/'.Str::slug($detail->category->title).'/'.$detail->category->id) }}">
                            {{ $detail->category->title }}  
                      </a>
                </div>  
            </div>
            @auth
            <!-- Add Comments -->
            <div class="card my-5">
                <h5 class="card-header">Add Comment</h5>
                <div class="card-body">
                    <form action="{{ url('save-comment/'.Str::slug($detail->title).'/'.$detail->id) }}" method="post">
                        @csrf
                        <textarea name="comment" class="form-control"></textarea>
                        <input type="submit" value="submit" class="btn btn-primary my-3">
                    </form>
                </div>
            </div>
            @endauth
            <!-- Get Comment -->
            <div class="card my-4">
                <h5 class="card-header">Comments <span class="badge badge-primary">{{ count($detail->comments) }}</span></h5>
                <div class="card-body">
                    @if($detail->comments)
                        @foreach($detail->comments as $comment)
                        <blockquote class="blockquote">
                            <p class="mb-0">{{ $comment->comment }}</p>
                            @if($comment->user_id == 0)
                                <footer class="blockquote-footer">Admin Web</footer>
                            @else
                                <footer class="blockquote-footer">{{ $comment->user->name }}</footer>
                            @endif
                        </blockquote>
                        <hr>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

        <!-- Right Sidebar -->
        <div class="col-md-4">
            <!-- Search -->
            <div class="card mb-3">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <form action="{{ route('home') }}">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q">
                            <div class="input-group-append">
                                <button class="btn btn-dark" type="button" id="button-addon2">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Recent Post -->
            <div class="card mb-3">
                <h5 class="card-header">Recent Post</h5>
                <div class="list-group list-group-flush">
                    @if($recent_posts)
                        @foreach($recent_posts as $post)
                            <a href="#" class="list-group-item">{{ $post->title }}</a>
                        @endforeach
                    @endif
                    <!-- <a href="#" class="list-group-item">Post 2</a>
                    <a href="#" class="list-group-item">Post 3</a> -->
                </div>
            </div>
            <!-- Popular Post -->
            <div class="card mb-3">
                <h5 class="card-header">Popular Post</h5>
                <div class="list-group list-group-flush">
                    @if($popular_posts)
                        @foreach($popular_posts as $post)
                            <a href="#" class="list-group-item">
                                {{ $post->title }}
                                <span class="badge badge-info float-right">{{ $post->views }}</span>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>
@endsection