<!-- Bootstrap core JavaScript-->
<script src="{{asset('backend')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{asset('backend')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('backend')}}/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="{{asset('backend')}}/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{asset('backend')}}/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('backend')}}/js/sb-admin.min.js"></script>

<!-- Dashboard -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{ asset('backend/js/scripts.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="{{ asset('backend/assets/demo/chart-area-demo.js') }}"></script>
<script src="{{ asset('backend/assets/demo/chart-bar-demo.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="{{ asset('backend/js/datatables-simple-demo.js') }}"></script>
