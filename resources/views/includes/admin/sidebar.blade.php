<ul class="sidebar navbar-nav">
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('admin.dashboard') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
        </a>
    </li>
    <!-- Category -->
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-list"></i>
        <span>Category</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item" href="{{ route('category.index') }}">View All</a>
        <a class="dropdown-item" href="{{ route('category.create') }}">Add New</a>
        </div>
    </li>
    <!-- Post -->
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-address-card"></i>
        <span>Post</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item" href="{{ route('post.index') }}">View All</a>
        <a class="dropdown-item" href="{{ route('post.create') }}">Add New</a>
        </div>
    </li>
    <!-- Comments -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('comments') }}">
        <i class="fas fa-fw fa-comments"></i>
        <span>Comments</span>
        </a>
    </li>
    <!-- Users -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('users') }}">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span>
        </a>
    </li>
     <!-- Settings -->
     <li class="nav-item">
        <a class="nav-link" href="{{ route('setting') }}">
        <i class="fas fa-fw fa-comments"></i>
        <span>Settings</span>
        </a>
    </li>
    <!-- Logout -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}">
        <i class="fas fa-fw fa-sign-out-alt"></i>
        <span>Logout</span>
        </a>
    </li>
</ul>