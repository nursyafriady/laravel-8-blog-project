<!DOCTYPE html>
<html lang="en">
  <head>
    @include('includes.admin.meta')
    @include('includes.admin.style')
    @if(!Session::has('adminData'))
        <script type="text/javascript">
              window.location.href = "{{ route('login') }}"
        </script>
    @endif
  </head>
  <body id="page-top">
    @include('includes.admin.navbar')
    <div id="wrapper">
      <!-- Sidebar -->
      @include('includes.admin.sidebar')
      <div id="content-wrapper">
          <!-- @if(Session::has('adminData'))
              <p class="text-center">You Are Loggin in</p>
          @endif -->
          @yield('content')
        <!-- Sticky Footer -->
        <!-- @include('includes.admin.footer')   -->
      </div>
      <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    @include('includes.admin.script')  
  </body>
</html>