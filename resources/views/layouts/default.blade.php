<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.frontend.meta')
    @include('includes.frontend.style')
</head>
<body>
    <!-- Navbar -->
    @include('includes.frontend.navbar')

    <!-- Get Latest Post -->
    @yield('content')

    <!-- scripts -->
    @include('includes.frontend.script')
</body>
</html>