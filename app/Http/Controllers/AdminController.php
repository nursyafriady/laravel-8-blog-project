<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Admin, Post, Comment, User};

class AdminController extends Controller
{
    // Login View
    function login()
    {
        return view('backend.login');
    }

    // Submit Login
    function submit_login(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $userCheck = Admin::where([
            'username' => $request->username,
            'password' => $request->password
        ])->count();

        // dd($userCheck);

        if($userCheck > 0) {
            $adminData = Admin::where([
                'username' => $request->username,
                'password' => $request->password
            ])->first();
            session(['adminData' => $adminData]);
            return redirect('admin/dashboard');
        } else {
            return redirect('admin/login')->with('error', 'Invalid Username/Password');
        }
    }

    // Dashboard
    function dashboard()
    {
        $posts = Post::orderBy('id','desc')->get();
        return view('backend.dashboard', compact('posts'));
    }

    // Show All Comments
    function comments()
    {
        $comments = Comment::orderBy('id','desc')->get();
        return view('backend.comment.index', compact('comments'));
    }

    // delete comment
    function delete_comment($id)
    {
        Comment::where('id', $id)->delete();
        return redirect('admin/comment');
    }

    // Show All Users
    function users()
    {
        $users = User::orderBy('id','desc')->get();
        return view('backend.user.index', compact('users'));
    }

    // delete user
    function delete_user($id)
    {
        User::where('id', $id)->delete();
        return redirect('admin/user');
    }

    // Logout
    function logout()
    {
        session()->forget(['adminData']);
        return redirect('admin/login');
    }
}
