<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class HomeController extends Controller
{
    function home(Request $request) 
    {
        if($request->has('q')) {
            $q=$request->q;
            $posts = Post::where('title', 'like', '%' .$q. '%')->orderBy('id', 'desc')->paginate(3);
        } else {
            $posts = Post::orderBy('id', 'desc')->paginate(3);
        }

        return view ('home', compact('posts'));
    }

    // Post Detail Page
    function detail(Request $request, $slug, $postId)
    {
        $detail = Post::find($postId);
        return view ('detail', compact('detail'));
    }
}


