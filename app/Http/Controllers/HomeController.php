<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Post, Comment, Category};

class HomeController extends Controller
{
    function index(Request $request) 
    {
        if($request->has('q')) {
            $q=$request->q;
            $posts = Post::where('title', 'like', '%' .$q. '%')->orderBy('id', 'desc')->paginate(3);
        } else {
            $posts = Post::orderBy('id', 'desc')->paginate(3);
        }

        return view ('home', compact('posts'));
    }

    // Post Detail Page
    function detail(Request $request, $slug, $postId)
    {
        // Update Post Count
        Post::find($postId)->increment('views');
        $detail = Post::find($postId);
        return view ('detail', compact('detail'));
    }

    // All Categories
    function all_category()
    {
        $categories = Category::orderBy('id', 'desc')->get();
        // dd($categories);
        return view('categories', compact('categories'));
    }

    // All Posts according to the category
    function category(Request $request, $cat_slug, $cat_id)
    {
        $category = Category::find($cat_id);
        $posts = Post::where('category_id', $cat_id)->orderBy('id', 'desc')->paginate(2);
        return view('category', compact('posts', 'category'));
    }

    function save_comment(Request $request, $slug, $id)
    {
        $request->validate([
            "comment" => "required"
        ]);

        $data = new Comment;
        $data->user_id = $request->user()->id;
        $data->post_id = $id;
        $data->comment = $request->comment;
        $data->save();

        return redirect('detail/'.$slug.'/'.$id)->with('success', 'comment has been submitted');
    }

    // user submit post
    function save_post_form() 
    {
        $categories = Category::all();
        return view('save-post-form', compact('categories'));
    }

    // Save Data
    function save_post_data(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'category' => 'required',
            'detail' => 'required',
        ]);

        // Thumbnail
        if($request->hasFile('thumbnail')){
            $image1=$request->file('thumbnail');
            $reThumbImage=time().'.'.$image1->getClientOriginalExtension();
            $dest1=public_path('/imgs/thumbnail');
            $image1->move($dest1, $reThumbImage);
        }else{
            $reThumbImage = null;
        }

        // Full Image
        if($request->hasFile('full_image')){
            $image2=$request->file('full_image');
            $reFullImage=time().'.'.$image2->getClientOriginalExtension();
            $dest2=public_path('/imgs/fullimage');
            $image2->move($dest2, $reFullImage);
        }else{
            $reFullImage = null;
        }

        $post = new Post;
        $post->user_id = $request->user()->id;
        $post->title = $request->title;
        $post->category_id = $request->category;
        $post->detail = $request->detail;
        $post->tags = $request->tags;
        $post->thumbnail = $reThumbImage;
        $post->full_image = $reFullImage;
        $post->status = 1;
        $post->save();

        return redirect('save-post-form')->with('success','Post has been added');
    }

    // Manage Posts
    function manage_posts(Request $request)
    {
        $data = Post::where('user_id', $request->user()->id)->orderBy('id', 'desc')->get();
        return view('manage-posts', compact('data'));
    }
}


